package es.pildoras.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {
	
//		Creacion de Objetos de tipo Empleado
//		Empleados Empleado1 = new JefeEmpleado();
//		Empleados Empleado2 = new SecretarioEmpleado();
		
		
		
//		Uso de los objetos creados
//		System.out.println(Empleado1.getTareas());
//		System.out.println("  ");
//		System.out.println(Empleado2.getTareas());
		
//		CARGAMOS EL ARCHIVO XML
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("appContext.xml");
		
//		LE PEDIMOS AL ARCHIVO CARGADO EL BEAN, EL OBJETO (ALIAS DEL BEAN, INTERFAZ)
		Empleados Juan = contexto.getBean("miEmpleado", Empleados.class);
		
		System.out.println(Juan.getTareas());
		
//		CERRAMOS EL ARCHIVO XML PARA LIBERAR RECURSOS
		contexto.close();
		
		
		

	}

}
